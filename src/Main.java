import java.io.*;

public class Main{
    public static void main(String[] args){
        
        /* DEFINE N-GRAM SIZE AND FEATURE-SIZE HERE */
        int N_GRAM_SIZE = 4;
        int FEATURE_SIZE = 5000;
        
        /* CREATE TST */
        TST tst = new TST(FEATURE_SIZE, N_GRAM_SIZE);

        /* READ GIVEN FILES */
        String[] legitimate_train = readFile("legitimate-train.txt");
        String[] legitimate_test = readFile("legitimate-test.txt");
        String[] phishing_train = readFile("phishing-train.txt");
        String[] phishing_test = readFile("phishing-test.txt");

        /* CONSOLE OUTPUTS */
        System.out.println("Legitimate training file has been loaded with [" + legitimate_train.length + "] instances");
        System.out.println("Legitimate test file has been loaded with [" + legitimate_test.length + "] instances");
        System.out.println("Phishing training file has been loaded with [" + phishing_train.length + "] instances");
        System.out.println("Phishing test file has been loaded with [" + phishing_test.length + "] instances");

        /* PREPROCESS URL's */
        preprocess(legitimate_train);
        preprocess(legitimate_test);
        preprocess(phishing_train);
        preprocess(phishing_test);
        
        /* CREATE N-GRAMS FOR TRAIN SET AND INSERT TO TST */
        createNGrams(legitimate_train, tst, N_GRAM_SIZE, false); // false represents legitimate
        createNGrams(phishing_train, tst, N_GRAM_SIZE, true); // true represents phishing
       
        // Inorder traversal for all tst, calculates weight
        // of all n-grams, then append corresponding n-grams
        // to hashmaps to sort them by related field
        tst.traverse();

        // create strong_phishing_features file
        writeFile("strong_phishing_features.txt", tst.calculate_strong_phishing_features());
        // create strong_legitimate_features file
        writeFile("strong_legitimate_features.txt", tst.calculate_strong_legitimate_features());
        // create all_weights file
        writeFile("all_feature_weights.txt", tst.calculate_all_weights());

        /* DELETE UNSIGNIFICANT N-GRAMS FROM THE TREE */
        tst.delete_insignificant_features();
        
        /* TEST DATA */
        tst.testData(phishing_test, legitimate_test);
        
    }
   
    /* remove https http www from the url then lowercase */
    public static void preprocess(String[] arr){
        for(int i = 0; i < arr.length; i++)
        	arr[i] = arr[i].replace("https", "").replace("http", "").replace("www", "").toLowerCase();
            //arr[i] = arr[i].toLowerCase().replaceFirst("^https?\\:\\/\\/(www.)?", "");
    }
    
    /* FUNCTION TO CREATE N-GRAMS AND THEN INSERT THEM TO TST */
    public static void createNGrams(String[] arr, TST tst, int N_GRAM_SIZE, boolean isPhishing){
        for(int i = 0; i < arr.length; i++){
            for(int j = 0; j < arr[i].length() - N_GRAM_SIZE + 1; j++)
                tst.insert(arr[i].substring(j, j + N_GRAM_SIZE), isPhishing); //split each url by given N_GRAM_SIZE and insert them to TST
        }

        System.out.println("TST has been loaded with "+ arr.length + " n-grams");
    }
    
    /* FUNCTION TO READ FROM A FILE */
    public static String[] readFile(String file){
            try {
                String fileContent = "";
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line = br.readLine();
                while (line != null) { 
                    fileContent += line + "\n";
                    line = br.readLine();
                }
                br.close();

                String[] lines = fileContent.split("\n"); // split lines then append them to an array
                return lines;

            } catch (Exception e) {
                System.out.print("Error while reading " + file + "\n");
            }

            return null;
    }
    
    /* FUNCTION TO WRITE TO A FILE */
    public static void writeFile(String file, String output){
        try{
            File outputF = new File(file);
            FileWriter writer = new FileWriter(outputF);
            writer.write(output);
            writer.close();
        }catch(Exception e){
            System.out.print("Error while writing to " + file + "\n");
        }
    }
    
}
