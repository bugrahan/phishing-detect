import java.util.*;

public class TST {

    class Node{
        char c; // character data
        Node lo, eq, hi; // left, middle and right pointers
        int phish_occurrence, legitimate_occurrence;
        float weight; 
        boolean flag; // to represent end of string

        public Node(char c){
            this.c = c;
        }
    }
    
    Node root;
    int FEATURE_SIZE;
    int N_GRAM_SIZE;
    int tree_size;

    public TST(int feature_size, int n_gram_size){
        this.FEATURE_SIZE = feature_size;
        this.N_GRAM_SIZE = n_gram_size;

        System.out.println("n-gram based phishing detection via TST\n");
    }

    public void insert(String str, boolean isPhishing){
        root = insert(root, str, 0, isPhishing);
    }

    private Node insert(Node root, String str, int idx, boolean isPhishing){
        char c = str.charAt(idx);
        if (root == null)
            root = new Node(c);
        
        if (c > root.c)
            root.hi  = insert(root.hi, str, idx, isPhishing);
        else if (c < root.c)
            root.lo = insert(root.lo, str, idx, isPhishing);
        else if (idx < str.length() - 1)
            root.eq = insert(root.eq, str, idx+1, isPhishing);
        else{
            if(isPhishing) // if n-gram comes from phishing.txt then increase phish_occurence by 1
                root.phish_occurrence++;
            else // otherwise n-gram comes from legitimate.txt; increase legitimate_occurence by 1
                root.legitimate_occurrence++;

            root.flag = true; // end of string
        }
        
        return root;
    }

    public Node get(String str){
        return get(root, str, 0);
    }
    
    /* GET LAST NODE OF A N-GRAM */
    private Node get(Node root, String str, int idx){
        if (root == null) 
            return null;
        char c = str.charAt(idx);
        if (c > root.c)
            return get(root.hi,  str, idx);
        else if (c < root.c)
            return get(root.lo, str, idx);
        else if (idx < str.length() - 1)
            return get(root.eq,   str, idx+1);
        else if(root.flag) // if it's end of the word
            return root;
        else
            return null;
    }

    /* Delete a n-gram from the TST */
    public void delete(String str){
        Node n = get(str); // get n-gram's last node
        if(n != null){ // if n-gram exists
            n.flag = false; // set end of string as false, so it's not a valid word anymore
            tree_size--; // shrink TST by 1
        }
    }

    /* CALCULATES WEIGHT OF GIVEN NODE(N-GRAM'S LAST NODE) */
    private void calculateWeight(Node n){
        int LO = n.legitimate_occurrence;
        int PO = n.phish_occurrence;
        if (PO > 0 && LO == 0)
            n.weight = 1; // a very unique n-gram for phishing medium
        else if (PO == 0 && LO > 0)
            n.weight = -1; // a very unique n-gram for legitimate medium
        else if (PO > 0 && LO > 0){
            if (PO > LO)
                n.weight = (float)Math.min(PO, LO) / (float)Math.max(PO, LO); // (0,1)
            else if(PO < LO)
                n.weight = -(float)Math.min(PO, LO) / (float)Math.max(PO, LO); // (-1,0)
            else 
                n.weight = 0; //the n-gram appears equally in both of the mediums
        }
    }

    // Get n-grams from TST, put it hashmaps and sort them by related fields
    // These hashmaps used only sorting and printing to a file
    // built by using inorder traversal of the original TST
    private HashMap<String, Integer> phishing_features = new HashMap<String, Integer>();
    private HashMap<String, Integer> legitimate_features = new HashMap<String, Integer>();
    private HashMap<String, Float> all_weights = new HashMap<String, Float>();

    public void traverse(){
    	traverse(root, "");
    }

    /* INORDER TRAVERSAL OF TREE -> CALCULATE WEIGHTS, FIND STRONGEST FEATURES (FOR BOTH PHISHING AND LEGITIMATE) 
        AND APPEND FOUND STRONGEST FEATURES TO CORRESPONDING HASHMAPS TO SORT IT LATER
        AT THE SAME TIME FIND HOW MANY N-GRAMS IN TST
    */
    private void traverse(Node root, String str){
        if(root == null)
            return;

        if(root.flag) { // if end of the word
        	if(root.phish_occurrence != 0) // if it's a phishing n-gram
        		phishing_features.put(str + root.c, root.phish_occurrence); //put strong_phishing_features hashmap
        	
        	if(root.legitimate_occurrence != 0) // if it's a legitimate n-gram
        		legitimate_features.put(str + root.c, root.legitimate_occurrence); //put strong_legitimate_features hashmap
        	
        	calculateWeight(root); 
        	all_weights.put(str + root.c, root.weight); 
        	tree_size++; 
        }
        
        traverse(root.lo, str);
        traverse(root.eq, str + root.c);
        traverse(root.hi, str);
        
    }

    // Sorts all_weights hashmap and returns as a string
    public String calculate_all_weights(){
        StringBuilder sb = new StringBuilder(); // put output content to a string builder
        sb.append("All N-Gram Weights\n");
        
        // sort weights
        all_weights.entrySet().stream()
        .sorted((w1, w2) -> -w1.getValue().compareTo(w2.getValue()))
        .forEach(w -> sb.append(w.getKey()).append(" - weight: ").append(w.getValue()).append("\n"));

        System.out.println(tree_size +" n-grams + weights have been saved to the file \"all_feature_weights.txt\"");

        return sb.toString(); // return all_feature_weights.txt's content
    }
    
    // to temporarily store significant_features (comes from both phishing and legitimate)
    private Set<String> significant_features = new HashSet<String>();
    
    public String calculate_strong_phishing_features(){

        StringBuilder sb = new StringBuilder(); // put output content to a string builder
        sb.append("Most important phishing n_grams\n");
        
        int[] i = {0}; // to avoid lambda expression effective final warning
        
        /* SORT HASHMAP THEN FIND STRONG COMPONENTS */
        phishing_features.entrySet().stream()
        .sorted((p1, p2) -> -p1.getValue().compareTo(p2.getValue()))
        .forEach(p -> {
        	if(i[0] < FEATURE_SIZE){
        		sb.append(++i[0]).append(". ").append(p.getKey()).append(" - freq: ").append(p.getValue()).append("\n");
        		significant_features.add(p.getKey()); // put it to significant_Features
        	}
        });
    	
        System.out.println(FEATURE_SIZE+" strong phishing n-grams have been saved to the file \"strong_phishing_features.txt\"");
        
        /* WE ARE DONE WITH SORTING AND PRINTING; CLEAR HASHMAP */
        phishing_features.clear();
        
        return sb.toString(); // return strong_phishing_features.txt's content
    }
    
    public String calculate_strong_legitimate_features(){
    	//Create legitimate.txt
        StringBuilder sb = new StringBuilder(); // put output content to a string builder
        sb.append("Most important legitimate n_grams\n");
        
        int[] i = {0}; // to avoid lambda expression effective final warning
        
        /* SORT HASHMAP THEN FIND STRONG COMPONENTS */
        legitimate_features.entrySet().stream()
        .sorted((l1, l2) -> -l1.getValue().compareTo(l2.getValue()))
        .forEach(l-> {
        	if(i[0] < FEATURE_SIZE){ // if it's a strong feature append it to file content
        		sb.append(++i[0]).append(". ").append(l.getKey()).append(" - freq: ").append(l.getValue()).append("\n");
        		significant_features.add(l.getKey()); // put it to significant_Features
        	}
        });
        
        System.out.println(FEATURE_SIZE+" strong legitimate n-grams have been saved to the file \"strong_legitmate_features.txt\"");
    	
        /* WE ARE DONE WITH SORTING AND PRINTING; CLEAR HASHMAP */
        legitimate_features.clear();
        
        return sb.toString(); // return strong_legitimate_features.txt's content
    }
    
    /* METHOD TO REMOVE UNSIGNIFICANT FEATURES FROM THE TREE */
    public void delete_insignificant_features(){
    	
    	int old_size = tree_size; // keep old size to compare with deleted ones
    	
    	for(String s : all_weights.keySet())
    		if(!significant_features.contains(s)) // if not a significant
    			delete(s); // delete from the tree
    
    	// We are done with helper structures clear them
    	all_weights.clear();
    	significant_features.clear();
    	
    	System.out.println((old_size - tree_size) + " insignificant n-grams have been removed from the TST");
    }

    public void testData(String[] phishing_test, String[] legitimate_test){
        int TP = 0, FN = 0, TN = 0, FP = 0, UP = 0, UL = 0;

        /* TEST PHISHING */
        for(int i = 0; i < phishing_test.length; i++){ // phishing test urls
            float weight = 0; // keep total weight of url here
            for(int j = 0; j < phishing_test[i].length() - N_GRAM_SIZE + 1; j++){ // create n-grams
                Node tmp = get(phishing_test[i].substring(j, j + N_GRAM_SIZE)); // search from the tree
                if(tmp != null) // if current extracted n-gram is in tree
                    weight += tmp.weight;
            }
        
            if(weight == 0) // unpredictable phishing 
                UP++;
            else if(weight > 0) // URL is from phishing and weight > 0 so it's a phishing URL
                TP++;
            else // URL is from phishing but weight < 0 so it's a legitimate URL
                FN++; 
        }

        /* TEST LEGITIMATE */
        for(int i = 0; i < legitimate_test.length; i++){ // legitimate test urls
            float weight = 0; // keep total weight of url here
            for(int j = 0; j < legitimate_test[i].length() - N_GRAM_SIZE + 1; j++){ // create n-grams
                Node tmp = get(legitimate_test[i].substring(j, j + N_GRAM_SIZE)); // search from tree
                if(tmp != null) // if current extracted n-gram is in tree
                    weight += tmp.weight;
            }
         
            if(weight == 0) // unpredictable legitimate weight is 0
            	UL++;
            else if(weight < 0) // URL is from legitimate and weight < 0 so it's a legitimate URL
            	TN++;
            else // URL is from legitimate but weight > 0 so it's a phishing URL
                FP++;
        }

        float accuary = (float)(TP + TN) / (float)(TP + TN + FP + FN + UP + UL);
        
        System.out.println("TP:" + TP + " FN:" + FN + " TN:" + TN + " FP:" + FP + " Unpredictable Phishing:" + UP + " Unpredictable Legitimate:" + UL + "\nAccuary:" + accuary);
    }
    
}
